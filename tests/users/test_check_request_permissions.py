import pytest
from rest_framework import exceptions
from users.requests_permissions import RequestsPermissions
from tests.test_utils import dicts_test_utils
from tests.test_utils import create_objects_test_utils

@pytest.mark.django_db
class TestCheckRequestPermissions:
    def setup_method(self):
        
        self.university = create_objects_test_utils.create_test_university(dicts_test_utils.university_dict_1)
        self.user = create_objects_test_utils.create_test_university_admin_user(dicts_test_utils.university_user_dict_1, self.university)
        self.university_2 = create_objects_test_utils.create_test_university(dicts_test_utils.university_dict_2)
        self.user_2 = create_objects_test_utils.create_test_university_user(dicts_test_utils.university_user_dict_2, self.university_2)


    def test_user_has_permission(self):
        user_types_with_permission = ['university_admin']
        request_university_id = self.university.id
        RequestsPermissions.check_request_permissions(self.user, user_types_with_permission, request_university_id)

        

    def test_user_does_not_have_permission(self):
        user_types_with_permission = ['super_user']
        with pytest.raises(exceptions.AuthenticationFailed) as exc_info:
            RequestsPermissions.check_request_permissions(self.user_2, user_types_with_permission, self.university_2.id)
        assert str(exc_info.value) == 'This User does not have permission.'

    def test_university_user_has_permission(self):
        user_types_with_permission = ['university_user']
        RequestsPermissions.check_request_permissions(self.user_2, user_types_with_permission, self.university_2.id)

        

    def test_university_user_does_not_have_permission(self):
        user_types_with_permission = ['university_user']
        with pytest.raises(exceptions.AuthenticationFailed) as exc_info:
            RequestsPermissions.check_request_permissions(self.user_2, user_types_with_permission, self.university.id)
        assert str(exc_info.value) == 'This User does not have permission.'